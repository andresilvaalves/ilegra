# # Build stage #
FROM maven:3.8.1-openjdk-16-slim AS builder
LABEL stage=builder
LABEL build=$BUILD_ID
WORKDIR /app
COPY pom.xml .

COPY src ./src
RUN mvn clean -e -B package



FROM openjdk:16-alpine as runtime
MAINTAINER André Silva Alves <andresilvaalves@gmail.com>

RUN mkdir -m 0755 -p /opt/app/ && mkdir -m 0755 -p /opt/app/log/ && mkdir -m 0755 -p /opt/app/data/ && mkdir -m 0755 -p /opt/app/data/in/ && mkdir -m 0777 -p /opt/app/data/out/

COPY --from=builder /app/target/*.jar /opt/app/application.jar

RUN chmod  0755 /opt/app/application.jar

WORKDIR /opt/app/

ENTRYPOINT ["java", "-Djava.net.preferIPv4Stack=true", "-Djava.net.preferIPv4Addresses=true", "-Dspring.profiles.active=prod", "-jar","/opt/app/application.jar"]
