package io.andresilvaalves.test.ilegra.customer;

import io.andresilvaalves.test.ilegra.commons.FileDTO;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(of = { "cnpj", "name" })
@ToString(of = {"name", "cnpj", "bussinesArea"})
@Builder
@Data
public class CustomerDTO implements FileDTO {

    private String cnpj;

    private String name;

    private String bussinesArea;

}
