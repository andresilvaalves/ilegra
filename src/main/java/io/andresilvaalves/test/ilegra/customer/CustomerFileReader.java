package io.andresilvaalves.test.ilegra.customer;

import io.andresilvaalves.test.ilegra.commons.FileDTO;
import io.andresilvaalves.test.ilegra.commons.FileReader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("customer")
public class CustomerFileReader implements FileReader<String[]> {

    public static final int FIELD_CNPJ = 1;
    public static final int FIELD_NAME = 2;
    public static final int FIELD_BUSINESS_AREA = 3;

    @Override
    public FileDTO reader(final String[] line) {

        return CustomerDTO.builder()
                          .cnpj(line[FIELD_CNPJ])
                          .name(line[FIELD_NAME])
                          .bussinesArea(line[FIELD_BUSINESS_AREA])
                          .build();

    }
}
