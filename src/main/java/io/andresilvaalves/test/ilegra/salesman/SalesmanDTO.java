package io.andresilvaalves.test.ilegra.salesman;

import io.andresilvaalves.test.ilegra.commons.FileDTO;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;

@ToString(of = { "name", "cpf", "salary" })
@EqualsAndHashCode(of = {"cpf", "name" })
@Builder
@Data
public class SalesmanDTO implements FileDTO {

    private String cpf;

    private String name;

    private BigDecimal salary;

}
