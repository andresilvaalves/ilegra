package io.andresilvaalves.test.ilegra.salesman;

import io.andresilvaalves.test.ilegra.commons.FileDTO;
import io.andresilvaalves.test.ilegra.commons.FileReader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@Qualifier("salesman")
public class SalesmanFileReader implements FileReader<String[]> {

    public static final int FIELD_CPF = 1;
    public static final int FIELD_NAME = 2;
    public static final int FIELD_SALARY = 3;

    @Override
    public FileDTO reader(final String[] linha) {
        return SalesmanDTO.builder()
                          .cpf(linha[FIELD_CPF])
                          .name(linha[FIELD_NAME])
                          .salary(new BigDecimal(linha[FIELD_SALARY]))
                          .build();
    }
}
