package io.andresilvaalves.test.ilegra;

import io.andresilvaalves.test.ilegra.processor.Watcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    @Autowired
    private Watcher directoryWatcher;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Bean
    public CommandLineRunner run() {
        return args -> {
            directoryWatcher.watch();
        };
    }


}
