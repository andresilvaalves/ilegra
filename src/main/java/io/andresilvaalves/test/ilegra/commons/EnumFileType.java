package io.andresilvaalves.test.ilegra.commons;

import io.andresilvaalves.test.ilegra.customer.CustomerFileReader;
import io.andresilvaalves.test.ilegra.sale.SaleFileReader;
import io.andresilvaalves.test.ilegra.salesman.SalesmanFileReader;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public enum EnumFileType {

    SALESMAN("001") {
        @Override
        public FileReader<String[]> getFileReader() {
            return new SalesmanFileReader();
        }
    },
    CUSTOMER("002") {
        @Override
        public FileReader<String[]> getFileReader() {
            return new CustomerFileReader();
        }
    },
    SALE("003") {
        @Override
        public FileReader<String[]> getFileReader() {
            return new SaleFileReader();
        }
    };

    EnumFileType(String code){
        this.code = code;
    }

    private final String code;

    public String getCode() {
        return code;
    }

    public abstract FileReader getFileReader();

    public static EnumFileType findBy(@NonNull String codeOrName){
        return Stream.of(EnumFileType.values())
                     .filter(f -> StringUtils.equalsAnyIgnoreCase(codeOrName, f.getCode(), f.name()))
                     .findFirst()
                     .orElse(null);
    }
}
