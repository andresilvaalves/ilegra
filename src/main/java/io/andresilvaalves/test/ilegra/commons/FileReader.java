package io.andresilvaalves.test.ilegra.commons;

public interface FileReader<T> {

    FileDTO reader(final T line);
}
