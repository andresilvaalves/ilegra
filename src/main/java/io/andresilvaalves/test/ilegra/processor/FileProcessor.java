package io.andresilvaalves.test.ilegra.processor;

import io.andresilvaalves.test.ilegra.commons.EnumFileType;
import io.andresilvaalves.test.ilegra.commons.FileDTO;
import io.andresilvaalves.test.ilegra.customer.CustomerDTO;
import io.andresilvaalves.test.ilegra.sale.SaleDTO;
import io.andresilvaalves.test.ilegra.salesman.SalesmanDTO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.apache.commons.io.FileUtils.readLines;

@Slf4j
public class FileProcessor extends Thread {

    private static final String EXTENSION = "dat";
    private static final String FIELD_DELIMITER = "ç";
    private static final String ENCODING_UTF_8 = "UTF-8";

    private final File file;
    private final String outputPath;

    private final Comparator<SaleDTO> comparatorSale = Comparator.comparing(SaleDTO::getTotal);

    private final Set<SalesmanDTO> salesman = new HashSet<>();
    private final Set<CustomerDTO> customer = new HashSet<>();
    private final Set<SaleDTO> sale = new HashSet<>();

    public FileProcessor(@NonNull File file, @NonNull String outputPath) {
        this.file = file;
        this.outputPath = outputPath;
    }

    @Override
    public void run() {
        final String fileExtension = FilenameUtils.getExtension(file.getName());

        if (StringUtils.equalsIgnoreCase(EXTENSION, fileExtension)) {
            log.info("Input file: {}", file);
            processInputFile();
        }
    }

    private void processInputFile() {

        try {
            List<String> lines = readLines(this.file, ENCODING_UTF_8);

            lines.parallelStream().forEach(line -> {
                log.debug("LINE: {}", line);

                String[] splitLine = StringUtils.split(line, FIELD_DELIMITER);
                EnumFileType enumTipoDados = EnumFileType.findBy(splitLine[0]);
                FileDTO fileDTO = enumTipoDados.getFileReader().reader(splitLine);

                if (fileDTO instanceof SalesmanDTO) {
                    salesman.add((SalesmanDTO) fileDTO);
                } else if (fileDTO instanceof CustomerDTO) {
                    customer.add((CustomerDTO) fileDTO);
                } else if (fileDTO instanceof SaleDTO) {
                    sale.add((SaleDTO) fileDTO);
                }
            });

            processOutputFile();

        } catch (IOException e) {
            log.error("Error processing file", e);
        }
    }

    private void processOutputFile() throws IOException {
        final String outputFile = outputPath + "/" + StringUtils.lowerCase(file.getName()) + ".done.dat";
        log.info("Output file: {} ", outputFile);

        StringBuilder sb = new StringBuilder();
        sb.append("Number of Customers: ").append(customer.size()).append("\n");
        sb.append("Number of Salesman: ").append(salesman.size()).append("\n");
        sb.append("ID Most Expensive Sale: ").append(getIdMostExpensiveSale()).append("\n");
        sb.append("Worst Salesman: ").append(getWorstSalesman()).append("\n");

        File file = new File(outputFile);
        FileUtils.writeStringToFile(file, sb.toString(), ENCODING_UTF_8);
    }

    private String getWorstSalesman() {

        AtomicReference<String> worstSalesman = new AtomicReference<>("-");
        AtomicReference<BigDecimal> sumMinimunSale = new AtomicReference<>(new BigDecimal("9999999999999999999"));

        Map<String, List<SaleDTO>> collect = this.sale.stream()
                                                      .collect(Collectors.groupingBy(SaleDTO::getSalesmanName));

        collect.forEach((key, value) -> {
            BigDecimal sumSale = value.stream().map(SaleDTO::getTotal).reduce(BigDecimal.ZERO, BigDecimal::add);

            if (sumSale.compareTo(sumMinimunSale.get()) < 0) {
                worstSalesman.set(key);
            }
            sumMinimunSale.set(sumMinimunSale.get().min(sumSale));
        });
        return worstSalesman.get();
    }

    private Long getIdMostExpensiveSale() {
        Long saleId = -1L;
        Optional<SaleDTO> maxSale = sale.stream().max(comparatorSale);
        if (maxSale.isPresent()) {
            saleId = maxSale.get().getSaleId();
        }
        return saleId;
    }

}