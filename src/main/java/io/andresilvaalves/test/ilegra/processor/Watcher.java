package io.andresilvaalves.test.ilegra.processor;

public interface Watcher {

    void watch() throws Exception;
}
