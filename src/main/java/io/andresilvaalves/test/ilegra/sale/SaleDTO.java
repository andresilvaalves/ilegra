package io.andresilvaalves.test.ilegra.sale;

import io.andresilvaalves.test.ilegra.commons.FileDTO;
import io.andresilvaalves.test.ilegra.item.ItemDTO;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@ToString(of = { "saleId", "items", "salesmanName" })
@Builder
@Data
public class SaleDTO implements FileDTO {

    private Long saleId;

    private String salesmanName;

    private List<ItemDTO> items;

    public BigDecimal getTotal() {
        return items.stream()
                    .map(item -> item.getPrice().multiply(new BigDecimal(item.getQuantity())))
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
