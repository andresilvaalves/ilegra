package io.andresilvaalves.test.ilegra.sale;

import io.andresilvaalves.test.ilegra.commons.FileDTO;
import io.andresilvaalves.test.ilegra.commons.FileReader;
import io.andresilvaalves.test.ilegra.item.ItemDTO;
import io.andresilvaalves.test.ilegra.item.ItemFileReader;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Qualifier("sale")
public class SaleFileReader implements FileReader<String[]> {

    public static final int FIELD_SALE_ID = 1;
    public static final int FIELD_ITEMS = 2;
    public static final int FIELD_SALESMAN_NAME = 3;
    protected final String FIELD_DELIMITER = ",";

    @Override
    public FileDTO reader(final String[] line) {

        return SaleDTO.builder()
                      .saleId(Long.valueOf(line[FIELD_SALE_ID]))
                      .items(getItemsSale(line[FIELD_ITEMS]))
                      .salesmanName(line[FIELD_SALESMAN_NAME])
                      .build();
    }

    private List<ItemDTO> getItemsSale(String itemSale) {

        itemSale = StringUtils.removeStart(itemSale, "[");
        itemSale = StringUtils.removeEnd(itemSale, "]");

        String[] itemsSale = StringUtils.split(itemSale, FIELD_DELIMITER);

        return Stream.of(itemsSale)
                     .map(m -> (ItemDTO) new ItemFileReader().reader(m))
                     .collect(Collectors.toList());
    }
}
