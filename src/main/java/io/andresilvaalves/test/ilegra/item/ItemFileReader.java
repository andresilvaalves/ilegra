package io.andresilvaalves.test.ilegra.item;

import io.andresilvaalves.test.ilegra.commons.FileDTO;
import io.andresilvaalves.test.ilegra.commons.FileReader;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@Qualifier("item")
public class ItemFileReader implements FileReader<String> {

    private static final int FIELD_ITEM_ID = 0;
    private static final int FIELD_ITEM_QUANTITY = 1;
    private static final int FIELD_ITEM_PRICE = 2;
    private static final String FIELD_DELIMITER = "-";

    @Override
    public FileDTO reader(final String item) {
        String[] splitItem = StringUtils.split(item, FIELD_DELIMITER);
        return ItemDTO.builder()
                      .id(Long.valueOf(splitItem[FIELD_ITEM_ID]))
                      .quantity(Long.valueOf(splitItem[FIELD_ITEM_QUANTITY]))
                      .price(new BigDecimal(splitItem[FIELD_ITEM_PRICE]))
                      .build();
    }
}
