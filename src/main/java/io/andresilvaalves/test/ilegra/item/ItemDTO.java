package io.andresilvaalves.test.ilegra.item;

import io.andresilvaalves.test.ilegra.commons.FileDTO;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@ToString(of = { "id", "quantity", "price"})
@Builder
@Data
public class ItemDTO implements FileDTO {

    private Long id;

    private Long quantity;

    private BigDecimal price;
}
